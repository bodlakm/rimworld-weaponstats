﻿﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	public class OtherWeapon : Weapon
	{
		public OtherWeapon ()
		{
		}

		public new void fillFromThing(Thing th)
		{
			base.fillFromThing (th);
			foreach (StatModifier st in th.def.statBases) {
				switch (st.stat.ToString())
				{
				case "RangedWeapon_Cooldown":
					cooldown = th.GetStatValue (st.stat);
					break;
				case "Mass":
					mass = th.GetStatValue (st.stat);
					break;
				}
			}
		}
	}
}

