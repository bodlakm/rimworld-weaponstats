﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{	
	public class MainTabWindow_WeaponStats : MainTabWindow
	{
		private const int ROW_HEIGHT = 30;
		private const int PAWN_WIDTH = 20;
		private const int STAT_WIDTH = 60;
		private const int GOTO_WIDTH = 0;
		private const int LABEL_WIDTH = 200;
		private const int QUALITY_WIDTH = STAT_WIDTH;
		private const int ACCURACY_WIDTH = 3 * STAT_WIDTH;
		private const int DTYPE_WIDTH = 2 * STAT_WIDTH;

		private string sortProperty;
		private string sortOrder;

		private bool showEquipped = true;

		public Vector2 scrollPosition = Vector2.zero;
		private float scrollViewHeight;

		private enum WeaponsTab : byte
		{
			None,
			Ranged,
			Melee,
			Grenades,
			Other
		}

		private WeaponsTab curTab = WeaponsTab.Ranged;

		List<RangedWeapon> rangedList;
		List<MeleeWeapon> meleeList;
		List<GrenadeWeapon> grenadeList;
		List<OtherWeapon> otherList;

		public MainTabWindow_WeaponStats()
		{
			this.doCloseX = true;
			this.closeOnEscapeKey = true;
			this.doCloseButton = false;
			this.closeOnClickedOutside = true;
			this.sortProperty = "marketValue";
			this.sortOrder = "DESC";
		}

		private void DoRangedPage(Rect rect, int count, List<RangedWeapon> rangedList)
		{
			string[] rangedHeaders = new string[8] {"Quality", "HP", "DPS", "$", "Damage", "Range", "Cooldown", "Warmup"};
			Widgets.CheckboxLabeled (new Rect (rect.x, rect.y, 130, 30), "Show equipped", ref this.showEquipped, false);
			GUI.BeginGroup (rect);
			scrollViewHeight = count * ROW_HEIGHT + 100;
			Rect inRect = new Rect (0, 0, rect.width - 16, scrollViewHeight);
			Rect scrollRect = new Rect (rect.x, rect.y, rect.width, rect.height);
			Widgets.BeginScrollView (scrollRect, ref scrollPosition, inRect);
			int num = 0;
			int ww = PAWN_WIDTH + LABEL_WIDTH;
			DrawCommon (num, inRect.width);
			foreach (string h in rangedHeaders) {
				printCellSort (h, h, ww);
				ww += STAT_WIDTH;
			}
			printCell ("Accuracy", num, ww, ACCURACY_WIDTH);
			ww += ACCURACY_WIDTH;
			printCell ("DType", num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
			num++;
			foreach (RangedWeapon rng in rangedList) {
				DrawRangedRow (rng, num, inRect.width);
				num++;
			}
			Widgets.EndScrollView();
			GUI.EndGroup ();
		}

		private void DoMeleePage(Rect rect, int count, List<MeleeWeapon> meleeList)
		{
			string[] meleeHeaders = new string[6] {"Quality", "HP", "DPS", "$", "Damage", "Cooldown"};
			Widgets.CheckboxLabeled (new Rect (rect.x, rect.y, 130, 30), "Show equipped", ref this.showEquipped, false);
			GUI.BeginGroup (rect);
			scrollViewHeight = count * ROW_HEIGHT + 100;
			Rect inRect = new Rect (0, 0, rect.width - 16, scrollViewHeight);
			Rect scrollRect = new Rect (rect.x, rect.y, rect.width, rect.height);
			Widgets.BeginScrollView (scrollRect, ref scrollPosition, inRect);
			int num = 0;
			int ww = PAWN_WIDTH + LABEL_WIDTH;
			DrawCommon (num, inRect.width);
			foreach (string h in meleeHeaders) {
				printCellSort (h, h, ww);
				ww += STAT_WIDTH;
			}
			printCell ("DType", num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
			num++;
			foreach (MeleeWeapon ml in meleeList) {
				DrawMeleeRow (ml, num, inRect.width);
				num++;
			}
			Widgets.EndScrollView();
			GUI.EndGroup ();
		}

		private void DoGrenadePage(Rect rect, int count, List<GrenadeWeapon> grenadeList)
		{
			string[] grenadeHeaders = new string[9] {"Quality", "HP", "$", "Damage", "MaxRange", "Cooldown", "Warmup", "Radius", "Delay"};
			Widgets.CheckboxLabeled (new Rect (rect.x, rect.y, 130, 30), "Show equipped", ref this.showEquipped, false);
			GUI.BeginGroup (rect);
			scrollViewHeight = count * ROW_HEIGHT + 100;
			Rect inRect = new Rect (0, 0, rect.width - 16, scrollViewHeight);
			Rect scrollRect = new Rect (rect.x, rect.y, rect.width, rect.height);
			Widgets.BeginScrollView (scrollRect, ref scrollPosition, inRect);
			int num = 0;
			int ww = PAWN_WIDTH + LABEL_WIDTH;
			DrawCommon (num, inRect.width);
			foreach (string h in grenadeHeaders) {
				printCellSort (h, h, ww);
				ww += STAT_WIDTH;
			}
			printCell ("DType", num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
			num++;
			foreach (GrenadeWeapon g in grenadeList) {
				DrawGrenadeRow (g, num, inRect.width);
				num++;
			}
			Widgets.EndScrollView();
			GUI.EndGroup ();
		}

		private void DoOtherPage(Rect rect, int count, List<OtherWeapon> otherList)
		{
			string[] grenadeHeaders = new string[2] {"HP", "$"};
			Widgets.CheckboxLabeled (new Rect (rect.x, rect.y, 130, 30), "Show equipped", ref this.showEquipped, false);
			GUI.BeginGroup (rect);
			scrollViewHeight = count * ROW_HEIGHT + 100;
			Rect inRect = new Rect (0, 0, rect.width - 16, scrollViewHeight);
			Rect scrollRect = new Rect (rect.x, rect.y, rect.width, rect.height);
			Widgets.BeginScrollView (scrollRect, ref scrollPosition, inRect);
			int num = 0;
			int ww = PAWN_WIDTH + LABEL_WIDTH;
			DrawCommon (num, inRect.width);
			foreach (string h in grenadeHeaders) {
				printCellSort (h, h, ww);
				ww += STAT_WIDTH;
			}
			num++;
			foreach (OtherWeapon o in otherList) {
				DrawOtherRow (o, num, inRect.width);
				num++;
			}
			Widgets.EndScrollView();
			GUI.EndGroup ();
		}

		private void DrawCommon(int num, float w)
		{
			GUI.color = new Color (1f, 1f, 1f, 0.2f);
			Widgets.DrawLineHorizontal (0, ROW_HEIGHT * (num + 1), w);
			GUI.color = Color.white;
			Rect rowRect = new Rect (0, ROW_HEIGHT * num, w, ROW_HEIGHT);
			if (Mouse.IsOver (rowRect)) {
				GUI.DrawTexture (rowRect, TexUI.HighlightTex);
			}
		}

		private int DrawCommonButtons(int x, int rowNum, float rowWidth, Thing t)
		{
			if (Prefs.DevMode) {
				Rect tmpRec = new Rect (rowWidth - 20, ROW_HEIGHT * rowNum, 20, ROW_HEIGHT);
				if (Widgets.ButtonText (tmpRec, "D")) {
					Dialog_WeaponDebug dlg = new Dialog_WeaponDebug (t);
					Find.WindowStack.Add (dlg);
				}
			}
			if (Widgets.ButtonInvisible (new Rect (20, ROW_HEIGHT * rowNum, rowWidth, ROW_HEIGHT))) {
				RimWorld.Planet.GlobalTargetInfo gti = new RimWorld.Planet.GlobalTargetInfo (t);
				CameraJumper.TryJumpAndSelect (gti);
			}
			return 0;
		}

		private void DrawRangedRow(RangedWeapon t, int num, float w)
		{
			DrawCommon (num, w);
			int ww = 0;
			ww = this.DrawCommonButtons (ww, num, w, t.thing);
			if (t.pawn != null) printCell ("(E)", num, ww, PAWN_WIDTH, t.pawn);
			ww += PAWN_WIDTH;
			printCell (t.label, num, ww, LABEL_WIDTH);
			ww += LABEL_WIDTH;
			printCell (t.quality, num, ww, QUALITY_WIDTH);
			ww += QUALITY_WIDTH;
			printCell (t.getHpStr(), num, ww);
			ww += STAT_WIDTH;
			printCell (t.getDps(), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.marketValue, 1), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.damage, 2), num, ww);
			ww += STAT_WIDTH;
			printCell (t.maxRange, num, ww);
			ww += STAT_WIDTH;
			printCell (t.cooldown, num, ww);
			ww += STAT_WIDTH;
			printCell (t.warmup, num, ww);
			ww += STAT_WIDTH;
			printCell (t.getAccuracyStr(), num, ww, ACCURACY_WIDTH);
			ww += ACCURACY_WIDTH;
			printCell (t.damageType, num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
		}

		private void DrawMeleeRow(MeleeWeapon t, int num, float w)
		{
			DrawCommon (num, w);
			int ww = 0;
			ww = this.DrawCommonButtons (ww, num, w, t.thing);
			if (t.pawn != null) printCell ("(E)", num, ww, PAWN_WIDTH, t.pawn);
			ww += PAWN_WIDTH;
			printCell (t.label, num, ww, LABEL_WIDTH);
			ww += LABEL_WIDTH;
			printCell (t.quality, num, ww, QUALITY_WIDTH);
			ww += QUALITY_WIDTH;
			printCell (t.getHpStr(), num, ww);
			ww += STAT_WIDTH;
			printCell (t.getDps(), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.marketValue, 1), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.damage, 2), num, ww);
			ww += STAT_WIDTH;
			printCell (t.cooldown, num, ww);
			ww += STAT_WIDTH;
			printCell (t.damageType, num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
		}

		private void DrawGrenadeRow(GrenadeWeapon t, int num, float w)
		{
			DrawCommon (num, w);
			int ww = 0;
			ww = this.DrawCommonButtons (ww, num, w, t.thing);
			if (t.pawn != null) printCell ("(E)", num, ww, PAWN_WIDTH, t.pawn);
			ww += PAWN_WIDTH;
			printCell (t.label, num, ww, LABEL_WIDTH);
			ww += LABEL_WIDTH;
			printCell (t.quality, num, ww, QUALITY_WIDTH);
			ww += QUALITY_WIDTH;
			printCell (t.getHpStr(), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.marketValue, 1), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.damage, 2), num, ww);
			ww += STAT_WIDTH;
			printCell (t.maxRange, num, ww);
			ww += STAT_WIDTH;
			printCell (t.cooldown, num, ww);
			ww += STAT_WIDTH;
			printCell (t.warmup, num, ww);
			ww += STAT_WIDTH;
			printCell (t.explosionRadius, num, ww);
			ww += STAT_WIDTH;
			printCell (t.explosionDelay, num, ww);
			ww += STAT_WIDTH;
			printCell (t.damageType, num, ww, DTYPE_WIDTH);
			ww += DTYPE_WIDTH;
		}

		private void DrawOtherRow(OtherWeapon t, int num, float w)
		{
			DrawCommon (num, w);
			int ww = 0;
			ww = this.DrawCommonButtons (ww, num, w, t.thing);
			if (t.pawn != null) printCell ("(E)", num, ww, PAWN_WIDTH, t.pawn);
			ww += PAWN_WIDTH;
			printCell (t.label, num, ww, LABEL_WIDTH);
			ww += LABEL_WIDTH;
			printCell (t.getHpStr(), num, ww);
			ww += STAT_WIDTH;
			printCell (Math.Round(t.marketValue, 1), num, ww);
			ww += STAT_WIDTH;
		}

		private void printCell(string content, int rowNum, int x, int width = STAT_WIDTH, string tooltip = "")
		{
			Rect tmpRec = new Rect (x, ROW_HEIGHT * rowNum + 2, width, ROW_HEIGHT - 2);
			Widgets.Label (tmpRec, content);
			if (tooltip != null && tooltip != "") {
				TooltipHandler.TipRegion (tmpRec, tooltip);
			}
		}

		private void printCellSort(string sortProperty, string content, int x, int width = STAT_WIDTH)
		{
			Rect tmpRec = new Rect (x, 2, width, ROW_HEIGHT - 2);
			Widgets.Label (tmpRec, content);
			if (Widgets.ButtonInvisible (tmpRec)) {
				if (sortProperty == "$") {
					if (this.sortProperty == "marketValue") {
						if (this.sortOrder == "DESC") {
							this.sortOrder = "ASC";
						}
					} else {
						this.sortProperty = "marketValue";
						this.sortOrder = "DESC";
					}
				} else if (sortProperty == "DPS") {
					
				}
			}
		}

		private void printCell(float content, int rowNum, int x, int width = STAT_WIDTH)
		{
			printCell (content.ToString (), rowNum, x, width);
		}

		private void printCell(double content, int rowNum, int x, int width = STAT_WIDTH)
		{
			printCell (content.ToString (), rowNum, x, width);
		}

		private WeaponsTab getAppropriateTab(Thing th)
		{
			if (th.def.IsRangedWeapon) {
				bool IsGrenade = false;
				foreach (ThingCategoryDef tc in th.def.thingCategories) {
					if (tc.defName == "Grenades") {
						IsGrenade = true;
						break;
					}
				}
				return IsGrenade ? WeaponsTab.Grenades : WeaponsTab.Ranged;
			} else if (th.def.IsMeleeWeapon) {
				bool IsRealMelee = false;
				foreach (ThingCategoryDef tc in th.def.thingCategories) {
					if (tc.defName == "WeaponsMelee") {
						IsRealMelee = true;
						break;
					}
				}
				return IsRealMelee ? WeaponsTab.Melee : WeaponsTab.Other;
			} else {
				return WeaponsTab.Other;
			}
		}

		public override void DoWindowContents (Rect rect)
		{
			base.DoWindowContents (rect);
			rect.yMin += 35;
			rangedList = new List<RangedWeapon>();
			meleeList = new List<MeleeWeapon>();
			grenadeList = new List<GrenadeWeapon>();
			otherList = new List<OtherWeapon>();
			RangedWeapon tmpRanged;
			MeleeWeapon tmpMelee;
			GrenadeWeapon tmpGrenade;
			OtherWeapon tmpOther;
			int rangedCount = 0;
			int meleeCount = 0;
			int grenadeCount = 0;
			int otherCount = 0;
			WeaponsTab tb;
			foreach (Thing th in Find.VisibleMap.listerThings.ThingsInGroup(ThingRequestGroup.Weapon)) {
				tb = this.getAppropriateTab (th);
				switch (tb) {
				case WeaponsTab.Ranged:
					tmpRanged = new RangedWeapon ();
					tmpRanged.fillFromThing (th);
					rangedList.Add (tmpRanged);
					rangedCount++;
					break;
				case WeaponsTab.Melee:
					tmpMelee = new MeleeWeapon ();
					tmpMelee.fillFromThing (th);
					meleeList.Add (tmpMelee);
					meleeCount++;
					break;
				case WeaponsTab.Grenades:
					tmpGrenade = new GrenadeWeapon ();
					tmpGrenade.fillFromThing (th);
					grenadeList.Add (tmpGrenade);
					grenadeCount++;
					break;
				case WeaponsTab.Other:
					tmpOther = new OtherWeapon ();
					tmpOther.fillFromThing (th);
					otherList.Add (tmpOther);
					otherCount++;
					break;
				}
			}

			// Weapons equipped by colonists
			if (this.showEquipped) {
				foreach (Pawn pw in Find.VisibleMap.mapPawns.FreeColonists) {
					foreach (ThingWithComps pth in pw.equipment.AllEquipmentListForReading) {
						if (pth.def.IsRangedWeapon || pth.def.IsMeleeWeapon) {
							tb = this.getAppropriateTab (pth);
							switch (tb) {
							case WeaponsTab.Ranged:
								tmpRanged = new RangedWeapon ();
								tmpRanged.fillFromThing (pth);
								tmpRanged.pawn = pw.NameStringShort;
								rangedList.Insert (0, tmpRanged);
								rangedCount++;
								break;
							case WeaponsTab.Melee:
								tmpMelee = new MeleeWeapon ();
								tmpMelee.fillFromThing (pth);
								tmpMelee.pawn = pw.NameStringShort;
								meleeList.Insert (0, tmpMelee);
								meleeCount++;
								break;
							case WeaponsTab.Grenades:
								tmpGrenade = new GrenadeWeapon ();
								tmpGrenade.fillFromThing (pth);
								tmpGrenade.pawn = pw.NameStringShort;
								grenadeList.Insert (0, tmpGrenade);
								grenadeCount++;
								break;
							case WeaponsTab.Other:
								tmpOther = new OtherWeapon ();
								tmpOther.fillFromThing (pth);
								tmpOther.pawn = pw.NameStringShort;
								otherList.Insert (0, tmpOther);
								otherCount++;
								break;
							}
						}
					}
				}
			}

			rangedList = rangedList.OrderByDescending (o=>o.marketValue).ToList();
			meleeList = meleeList.OrderByDescending (o=>o.marketValue).ToList();
			grenadeList = grenadeList.OrderByDescending (o=>o.marketValue).ToList();
			otherList = otherList.OrderByDescending (o=>o.marketValue).ToList();

			Text.Font = GameFont.Small;
			Text.Anchor = TextAnchor.UpperLeft;
			GUI.color = Color.white;

			List<TabRecord> list = new List<TabRecord> ();
			list.Add (new TabRecord ("Ranged", delegate {
				this.curTab = WeaponStats.MainTabWindow_WeaponStats.WeaponsTab.Ranged;
			}, this.curTab == WeaponsTab.Ranged));
			list.Add (new TabRecord ("Melee", delegate {
				this.curTab = WeaponStats.MainTabWindow_WeaponStats.WeaponsTab.Melee;
			}, this.curTab == WeaponsTab.Melee));
			list.Add (new TabRecord ("Grenades", delegate {
				this.curTab = WeaponStats.MainTabWindow_WeaponStats.WeaponsTab.Grenades;
			}, this.curTab == WeaponsTab.Grenades));
			list.Add (new TabRecord ("Other", delegate {
				this.curTab = WeaponStats.MainTabWindow_WeaponStats.WeaponsTab.Other;
			}, this.curTab == WeaponsTab.Other));
			TabDrawer.DrawTabs (rect, list);
			WeaponsTab wpTab = this.curTab;
			switch (wpTab) {
			case WeaponsTab.Ranged:
				DoRangedPage (rect, rangedCount, rangedList);
				break;
			case WeaponsTab.Melee:
				DoMeleePage (rect, meleeCount, meleeList);
				break;
			case WeaponsTab.Grenades:
				DoGrenadePage (rect, grenadeCount, grenadeList);
				break;
			case WeaponsTab.Other:
				DoOtherPage (rect, otherCount, otherList);
				break;
			default:
				DoRangedPage (rect, rangedCount, rangedList);
				break;
			}
		}
	}
}
