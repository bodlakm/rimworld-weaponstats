﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	public class RangedWeapon : Weapon
	{
		/* RANGED:
			 * th.def.statBases = MaxHitPoints, Flammability, DeteriorationRate, Beauty, SellPriceFactor, WorkToMake, Mass,
			 * 	AccuracyTouch, AccuracyShort, AccuracyMedium, AccuracyLong, RangedWeapon_Cooldown
			 * th.def.weaponTags = [Gun]
			 *
		*/
		public float accuracyTouch { get; set; }
		public float accuracyShort { get; set; }
		public float accuracyMedium { get; set; }
		public float accuracyLong { get; set; }
		public float minRange { get; set; }
		public float maxRange { get; set; }
		public float warmup { get; set; }

		public float getDps()
		{
			return (float)Math.Round(damage / (cooldown + warmup), 2);
		}

		public string getAccuracyStr()
		{
			StringBuilder sb = new StringBuilder ();
			if (minRange > RNG_TOUCH || maxRange < RNG_TOUCH) {
				sb.Append (" - /");
			} else {
				sb.Append (" ").Append (Math.Round(accuracyTouch, 1).ToString ()).Append (" /");
			}
			if (minRange > RNG_SHORT || maxRange < RNG_SHORT) {
				sb.Append (" - /");
			} else {
				sb.Append (" ").Append (Math.Round(accuracyShort, 1).ToString ()).Append (" /");
			}
			if (minRange > RNG_MEDIUM || maxRange < RNG_MEDIUM) {
				sb.Append (" - /");
			} else {
				sb.Append (" ").Append (Math.Round(accuracyMedium, 1).ToString ()).Append (" /");
			}
			if (minRange > RNG_LONG || maxRange < RNG_LONG) {
				sb.Append (" -");
			} else {
				sb.Append (" ").Append (Math.Round(accuracyLong, 1).ToString ());
			}
			return sb.ToString ();
		}

		public new void fillFromThing(Thing th)
		{
			base.fillFromThing (th);
			foreach (StatModifier st in th.def.statBases) {
				switch (st.stat.ToString())
				{
				case "MarketValue":
					accuracyTouch = th.GetStatValue (st.stat) * 100;
					break;
				case "AccuracyTouch":
					accuracyTouch = th.GetStatValue (st.stat) * 100;
					break;
				case "AccuracyShort":
					accuracyShort = th.GetStatValue (st.stat) * 100;
					break;
				case "AccuracyMedium":
					accuracyMedium = th.GetStatValue (st.stat) * 100;
					break;
				case "AccuracyLong":
					accuracyLong = th.GetStatValue (st.stat) * 100;
					break;
				case "RangedWeapon_Cooldown":
					cooldown = th.GetStatValue (st.stat);
					break;
				case "Mass":
					mass = th.GetStatValue (st.stat);
					break;
				}
			}

			foreach (VerbProperties vp in th.def.Verbs) {
				if (vp.ToString().StartsWith("VerbProperties")) {
					warmup = vp.warmupTime;
					maxRange = vp.range;
					minRange = vp.minRange;
					damage = vp.projectileDef.projectile.damageAmountBase;
					damageType = vp.projectileDef.projectile.damageDef.label;
				}
			}
		}
	}
}
