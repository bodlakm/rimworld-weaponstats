﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	abstract public class Weapon
	{
		protected const int RNG_TOUCH = 4;
		protected const int RNG_SHORT = 15;
		protected const int RNG_MEDIUM = 30;
		protected const int RNG_LONG = 50;

		public bool visible = true;
		public string label { get; set; }
		public IntVec3 position { get; set; }
		public int hp { get; set; }
		public float damage { get; set; }
		public float marketValue { get; set; }
		public float mass { get; set; }
		public float cooldown { get; set; }
		public string damageType { get; set; }
		public Thing thing { get; set; }
		public string quality { get; set; }
		public string pawn { get; set; }

		public string getHpStr()
		{
			StringBuilder sb = new StringBuilder ();
			sb.Append (hp.ToString ()).Append ("%");
			return sb.ToString ();
		}

		public void fillFromThing(Thing th)
		{
			thing = th;
			marketValue = th.MarketValue;
			position = th.Position;
			label = th.def.label;
			hp = 100 * th.HitPoints / th.MaxHitPoints;
			marketValue = th.MarketValue;
			QualityCategory qc;
			bool hasQuality = th.TryGetQuality (out qc);
			if (hasQuality) {
				quality = qc.GetLabel ();
			} else {
				quality = "normal";
			}
		}
	}
}
