﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	public class GrenadeWeapon : Weapon
	{
		public GrenadeWeapon ()
		{
		}
		public float minRange { get; set; }
		public float maxRange { get; set; }
		public float warmup { get; set; }
		public int explosionDelay { get; set; }
		public float explosionRadius { get; set; }

		public new void fillFromThing(Thing th)
		{
			base.fillFromThing (th);
			foreach (StatModifier st in th.def.statBases) {
				switch (st.stat.ToString())
				{
				case "RangedWeapon_Cooldown":
					cooldown = th.GetStatValue (st.stat);
					break;
				case "Mass":
					mass = th.GetStatValue (st.stat);
					break;
				}
			}

			foreach (VerbProperties vp in th.def.Verbs) {
				if (vp.ToString().StartsWith("VerbProperties")) {
					warmup = vp.warmupTime;
					maxRange = vp.range;
					minRange = vp.minRange;
					damage = vp.projectileDef.projectile.damageAmountBase;
					damageType = vp.projectileDef.projectile.damageDef.label;
					explosionDelay = vp.projectileDef.projectile.explosionDelay;
					explosionRadius = vp.projectileDef.projectile.explosionRadius;
				}
			}
		}
	}
}

