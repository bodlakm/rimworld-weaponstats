﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	public class Dialog_WeaponDebug : Window
	{
		private Thing thing;

		public Dialog_WeaponDebug (Thing th)
		{
			this.doCloseX = true;
			this.closeOnEscapeKey = true;
			this.doCloseButton = true;
			this.closeOnClickedOutside = true;
			this.thing = th;
		}

		public override Vector2 InitialSize {
			get {
				return new Vector2 (700, 700);
			}
		}

		protected override void SetInitialSizeAndPosition ()
		{
			base.SetInitialSizeAndPosition ();
			this.windowRect.x = (float)UI.screenWidth - this.windowRect.width;
			this.windowRect.y = (float)(UI.screenHeight - 35) - this.windowRect.height;
		}

		public override void DoWindowContents (Rect rect)
		{
			rect.yMin += 35;
			Text.Font = GameFont.Small;
			Text.Anchor = TextAnchor.UpperLeft;
			GUI.color = Color.white;
			StringBuilder sb = new StringBuilder ();
			if (thing.def != null) {
				if (thing.def.thingCategories != null) {
					sb.Append ("--- thingCategories ---").AppendLine ();
					foreach (ThingCategoryDef tc in thing.def.thingCategories) {
						sb.Append (tc.defName).AppendLine ();
					}
				}
				if (thing.def.statBases != null) {
					sb.Append ("--- statBases ---").AppendLine ();
					foreach (StatModifier st in thing.def.statBases) {
						sb.Append (st.stat.ToString ()).Append (": ").Append (thing.GetStatValue (st.stat).ToString ()).AppendLine ();
					}
				}
				if (thing.def.weaponTags != null) {
					sb.Append ("--- weaponTags ---").AppendLine ();
					foreach (string wt in thing.def.weaponTags) {
						sb.Append (wt).AppendLine ();
					}
				}
				if (thing.def.Verbs != null) {
					sb.Append ("--- verbs ---").AppendLine ();
					ProjectileProperties proj = null;
					foreach (VerbProperties vp in thing.def.Verbs) {
						VerbCategory vcat = vp.category;
						sb.Append(vp.isPrimary ? "P" : "").Append ("+ ").Append (vp.verbClass.ToString ()).Append(" (").Append(vcat.ToString()).Append(")").AppendLine ();
						sb.Append ("++ ").Append (vp.ToString()).Append (": ").Append (vp.label).AppendLine ();
						if (vp.projectileDef != null && vp.projectileDef.projectile != null) {
							proj = vp.projectileDef.projectile;
						}
					}
					if (proj != null) {
						sb.Append ("--- projectile ---").AppendLine ();
						sb.Append ("damageDef: ").Append (proj.damageDef.label).AppendLine ();
						sb.Append ("damageAmountBase: ").Append (proj.damageAmountBase.ToString ()).AppendLine ();
						sb.Append ("speed: ").Append (proj.speed.ToString ()).AppendLine ();
						sb.Append ("explosionRadius: ").Append (proj.explosionRadius.ToString ()).AppendLine ();
					}
				}
			}

			GUI.BeginGroup (rect);
			Rect tmpRec = new Rect (0, 0, 700, 700);
			Widgets.Label (tmpRec, sb.ToString());
			GUI.EndGroup ();
		}
	}
}

