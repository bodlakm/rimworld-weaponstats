﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using Verse;
using RimWorld;

namespace WeaponStats
{
	public class MeleeWeapon : Weapon
	{
		/* MELEE:
			 * th.def.statBases = MaxHitPoints, Flammability, DeteriorationRate, Beauty, SellPriceFactor, WorkToMake, Mass,
			 * 	MeleeWeapon_DamageAmount, MeleeWeapon_Cooldown
			 * th.def.weaponTags = [Melee]
			*/
		public float getDps()
		{
			return (float)Math.Round(damage / cooldown, 2);
		}

		public new void fillFromThing(Thing th)
		{
			base.fillFromThing (th);
			foreach (StatModifier st in th.def.statBases) {
				switch (st.stat.ToString())
				{
				case "MeleeWeapon_Cooldown":
					cooldown = th.GetStatValue (st.stat);
					break;
				case "MeleeWeapon_DamageAmount":
					damage = th.GetStatValue (st.stat);
					break;
				case "Mass":
					mass = th.GetStatValue (st.stat);
					break;
				}
			}

			foreach (VerbProperties vp in th.def.Verbs) {
				if (vp.ToString ().StartsWith ("VerbProperties")) {
					damageType = vp.meleeDamageDef.defName;	
				}
			}
		}
	}
}
